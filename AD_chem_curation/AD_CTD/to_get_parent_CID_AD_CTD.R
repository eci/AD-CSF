#To get chemical information for the AD-CTD chemical list

#Install WebChem from GitHub, in case is already installed, please skip this step.
install.packages("devtools")
library("devtools")
install_github("ropensci/webchem")

#Load packages
library(webchem)
library(data.table)
library(tidyverse)
library(dplyr)


# Set the path of the working directory
local_dir <- "../../../../AD_chem_curation/AD_CTD"
setwd(local_dir)

# Read the input table with the chemical CID
pmid_data <-read.csv("AD.csv")
as.data.frame(pmid_data)

unique_cids <- unique(pmid_data$cid) #get the unique CID

write.csv(unique_cids,"unique_CIDs_AD_CTD.csv",row.names = F)

# Now, copy this info (unique_CIDs_AD_CTD.csv) into ID Exchange and get the parent CIDs
#https://pubchem.ncbi.nlm.nih.gov/idexchange/idexchange.cgi
#Select Input CIDs-> CIDs
#Operator type -> Parent CID
#Output IDs-> CID
#Output method -> two column
#Compression -> no compression
#Save the result table as a text file and read it in the next step: 

cid_to_parent <- read.delim("AD_CTD_PARENT_CID.txt",header=F)

unique_parent_cids <- na.exclude(unique(cid_to_parent$V2))
unique_parent_cids <- as.data.frame(unique_parent_cids)


# Get the information you need for the analysis with patRoon: 
# Parent_CID	Name	ExactMass	Molecular_Formula	XlogP	InChI	SMILES	InChIKey	IUPAC_Name
# To check more properties visit: https://pubchemdocs.ncbi.nlm.nih.gov/pug-rest#_Toc494865567

selected_properties <- c("Title","ExactMass","MolecularFormula","XlogP",
                         "InChI","IsomericSMILES","InChIKey","IUPACName")

# Retrieve info with webchem
CID_info_all <- as.data.table(webchem::pc_prop(unique_parent_cids, selected_properties))

# Save the chemical list as csv file to be used as suspect list in patRoon
write.csv(CID_info_all,"AD-CTD.csv",row.names = F) #This table is also shared in Zenodo

