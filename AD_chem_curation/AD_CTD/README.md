## Preamble

Here it is shown as an example the input files and code that were used to obtain the chemical information for the AD-CTD suspect list.

 - "AD.csv" : this is the list containing 185 CIDs related with AD in the CTD that will be used as input file.
 - "AD_CTD_PARENT_CID.txt": this is the list obtained after mapping to the parent CIDs via [PubChem ID Exchange](https://pubchem.ncbi.nlm.nih.gov/idexchange/idexchange.cgi).
 - "to_get_parent_CID_AD_CTD.R": this is the code used to get the chemical information of the AD-CTD suspect list.
 
The same procedure was followed for the rest of lists.