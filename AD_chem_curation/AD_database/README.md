## Preamble

Here it is shown how the AD-database was created and how all the scores from the different lists were summarized to get unique scores per CID. 
These are discussed in Talavera Andújar et al. (in prep.), SI S3.6.
      
 - "To_Create_AD_Database.R": Code to create the database
 - "AD_TABLE_SCORES_INFO.csv": This table contains the same CIDs than the AD-database (41,917) and some extra information such as the scores "Coocurrence" and "NoRecord". See Zaslavsky et. al [work](https://www.frontiersin.org/articles/10.3389/frma.2021.689059/full) for detailed information about the scores.
    